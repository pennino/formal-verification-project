import stainless.annotation._
import stainless.collection._
import stainless.lang._
import scala.language.postfixOps
import stainless.proof._


// typeclass for Field
abstract class Field[T] {
  def sum(a: T, b: T): T
  def mul(a: T, b: T): T
  def neg(a: T): T
  def inv(a: T): T
  def zero: T
  def one: T
  
  def sum(as: List[T]): T = as match {
    case x::xs => as.foldLeft(x)(sum)
    case Nil() => zero
  }
  
  def mul(as: List[T]): T = as match {
    case x::xs => as.foldLeft(x)(mul)
    case Nil() => one
  }
  
  // Would be good to have them define as sum(as: T*) too, not sure if possible in stainless
  
  def foldMul(a: T, n: BigInt): T = {
    if(n < 0) foldMul(inv(a), -n)
    else if(n == 0) one
    else if(n%2 == 0) foldMul(mul(a,a),n/2)
    else mul(foldMul(mul(a,a),(n-1)/2),a)
  }
  
  def foldSum(a: T, n: BigInt): T = {
    if(n < 0) foldSum(neg(a), -n)
    else if(n == 0) zero
    else if(n%2 == 0) foldSum(sum(a,a),n/2)
    else sum(foldSum(sum(a,a),(n-1)/2),a)
  }
  
  @law
  def oneIsNotZero() =
    zero != one
  
  @law
  def sumAssociativity(a: T, b: T, c:T) =
    sum(a,sum(b,c)) == sum(sum(a,b), c)
  
  @law
  def mulAssociativity(a: T, b: T, c:T) =
    mul(a,mul(b,c)) == mul(mul(a,b), c)
  
  @law
  def sumCommutativity(a: T, b: T) =
    sum(a,b) == sum(b,a)
  
  @law
  def mulCommutativity(a: T, b: T) =
    mul(a,b) == mul(b,a)
  
  @law
  def sumZero(a: T) =
     sum(a,zero) == a
  
  @law
  def mulOne(a: T) =
     mul(a,one) == a
  
  @law
  def sumNeg(a: T) =
    sum(a, neg(a)) == zero
  
  @law
  def mulInv(a: T) =
    a == zero || mul(a, inv(a)) == one
  
  @law
  def distributivity(a: T, b: T, c:T) =
    mul(a,sum(b,c)) == sum(mul(a,b), mul(a,c))
  
  def uniqueZero(a: T, b: T) = {
    require(sum(a,b)==b)
    assert(sumZero(a))                        // a = a+0
    assert(sumNeg(b))                         //   = a+(b-b)
    assert(sumAssociativity(a,b,neg(b)))      //   = (a+b)-b
  }.ensuring(a==zero)                         //   = b-b = 0
  
  def uniqueOne(a: T, b: T) = {
    require(b != zero && mul(a,b)==b)
    assert(mulOne(a))                         // a = a 1
    assert(mulInv(b))                         //   = a (b b^{-1})
    assert(mulAssociativity(a,b,inv(b)))      //   = (a b) b^{-1}
  }.ensuring(a == one)                        //   = b b^{-1} = 1
  
  def mulZero(a: T) = {
    assert(distributivity(a,a,zero))              // aa+a0 = a(a+0)
    assert(sumZero(a))                            //       = aa
    assert(sumCommutativity(mul(a,a),mul(a,zero)))// a0+aa = aa
    uniqueZero(mul(a,zero),mul(a,a))              // => a0=0
  }.ensuring(mul(a,zero) == zero)
  
  def mulNegOne(a: T) = {
    assert(mulCommutativity(neg(one), a))             //(-1)a = a(-1)
    assert(sumZero(mul(a,neg(one))))                  //      = a(-1)+0
    assert(sumNeg(a))                                 //      = a(-1)+(a-a)
    assert(mulOne(a))                                 //      = a(-1)+(a(1)-a)
    assert(sumAssociativity(mul(a,neg(one)),a,neg(a)))//      = (a(-1)+a(1))-a
    assert(distributivity(a,neg(one),one))            //      = a(-1+1)-a
    assert(sumCommutativity(one,neg(one)))            //      = a(1-1)-a
    assert(sumNeg(one))                               //      = a 0 -a
    mulZero(a)                                        //      = 0 - a
    assert(sumCommutativity(zero,neg(a)))             //      = -a + 0
    assert(sumZero(neg(a)))                           //      = -a
  }.ensuring(neg(a) == mul(neg(one), a))
  
  def zeroProd(a: T, b: T) = {
    require(mul(a,b) == zero)                         // b != 0 && a != 0 => a^{-1} and b^{-1} exists
    assert(mulInv(a))                                 // 1 = a a^{-1}
    assert(mulOne(mul(a,inv(a))))                     //   = (a a^{-1}) 1
    assert(mulInv(b))                                 //   = (a a^{-1}) (b b^{-1})
    assert(mulCommutativity(a,inv(a)))                //   = (a^{-1} a) (b b^{-1})
    assert(mulAssociativity(mul(inv(a),a), b, inv(b)))//   = ((a^{-1} a) b) b^{-1}
    assert(mulAssociativity(inv(a),a,b))              //   = (a^{-1} (ab)) b^{-1}
    mulZero(inv(a))                                   //   = 0 b^{-1}
    assert(mulCommutativity(zero,inv(b)))             //   = b^{-1} 0
    mulZero(inv(b))                                   //   = 0
    assert(oneIsNotZero())
  }.ensuring(a == zero || b == zero)
  
  def negZero() = {
    mulNegOne(zero)                       //-0 = (-1) 0
    mulZero(neg(one))                       //   = 0
  }.ensuring(neg(zero) == zero)
  
  def invOne() = {
    assert(oneIsNotZero())                  //
    assert(mulOne(inv(one)))                // 1^{-1} = 1^{-1} 1
    assert(mulCommutativity(inv(one),one))  //        = 1 1^{-1}
    assert(mulInv(one))                     //        = 1
  }.ensuring(inv(one) == one)
  
  def negNeg(a: T) = {
    assert(sumZero(a))                            // a = a + 0
    assert(sumNeg(neg(a)))                        //   = a + (-a -(-a))
    assert(sumAssociativity(a,neg(a),neg(neg(a))))//   = (a - a) -(-a)
    assert(sumNeg(a))                             //   = zero -(-a)
    assert(sumCommutativity(zero,neg(neg(a))))    //   = -(-a) + zero
    assert(sumZero(neg(neg(a))))                  //   = -(-a)
  }.ensuring(neg(neg(a)) == a)
  
  def InvIsNotZero(a: T) = {
    require(a != zero)
    assert(mulInv(a))               // a a^{-1} = 1
    assert(oneIsNotZero())          //         != 0
    mulZero(a)                      //          = a 0
  }.ensuring(inv(a) != zero)        // a^{-1} != 0
  
  def invInv(a: T) = {
    require(a != zero)
    InvIsNotZero(a)
    assert(mulOne(a))                             // a = a 1
    assert(mulInv(inv(a)))                        //   = a (a^{-1} (a^{-1})^{-1})
    assert(mulAssociativity(a,inv(a),inv(inv(a))))//   = (a a^{-1}) (a^{-1})^{-1}
    assert(mulInv(a))                             //   = 1 (a^{-1})^{-1}
    assert(mulCommutativity(one, inv(inv(a))))    //   = (a^{-1})^{-1} 1
    assert(mulOne(inv(inv(a))))                   //   = (a^{-1})^{-1}
  }.ensuring(a == inv(inv(a)))
  
  def mulNeg(a: T, b: T, c: T) = {
    mulNegOne(mul(a,b))                         // -(ab) = (-1) (ab)
    assert(mulAssociativity(neg(one),a,b))        //       = ((-1) a) b
    mulNegOne(a)                                //       = (-a) b
                                                  // -(ab) = (-1) (ab)
    assert(mulCommutativity(neg(one),mul(a,b)))   //       = (ab)(-1)
    assert(mulAssociativity(a,b,neg(one)))         //       = a (b(-1))
    assert(mulCommutativity(b,neg(one)))         //       = a ((-1) b)
    mulNegOne(b)                                //       = a (-b)
  }.ensuring(neg(mul(a,b)) == mul(neg(a),b) && neg(mul(a,b)) == mul(a,neg(b)))
  
  // p needs to be the characteristic of the field
  /*def freshman(a: T, b: T, p: BigInt) = {
  }.ensuring(pow(sum(a, b), p) == sum(pow(a, p), pow(b, p)))*/
}

abstract class VectorSpace[T, X]() {
  def F: Field[T]// sorry about the capital, but lower case f would feel very wrong here
  type V = X
  def sum(u: V, v: V): V
  def mul(a: T, u: V): V
  def neg(u: V): V
  def zero: V
  
  @law
  def associativity(u: V, v: V, w: V) =
    sum(u,sum(v,w)) == sum(sum(u,v),w)
  
  @law
  def commutativity(u: V, v: V) = 
    sum(u,v) == sum(v,u)
  
  @law
  def sumZero(u: V) = 
    sum(u,zero) == u
  
  @law
  def sumNeg(u: V) =
    sum(u,neg(u)) == zero
  
  @law
  def compatibility(a: T, b: T, u: V) =
    mul(a,mul(b,u)) == mul(F.mul(a,b), u)
  
  @law
  def mulOne(u: V) =
    u == mul(F.one, u)
  
  @law
  def distributivity1(a: T, u: V, v: V) =
    mul(a,sum(u,v)) == sum(mul(a,u), mul(a,v))
  
  @law 
  def distributivity2(a: T, b: T, u: V) =
    mul(F.sum(a,b),u) == sum(mul(a,u),mul(b,u))

  /*def uniqueZero(u: V, v: V) = {
    require(sum(u, v) == u)
    assert(sumZero(v))                        // v = v+0
    assert(sumNeg(u))                         //   = v+(u-u)
    assert(associativity(v, u, neg(u)))       //   = (v+u)-u
  }.ensuring(v == zero)                       //   = u-u = 0

  def mulZero(u: V) = {
    assert(distributivity2(F.zero, F.one, u))         // (1+0)*u = 1*u + 0*u
    assert(F.sumCommutativity(F.zero, F.one))
    assert(F.sumZero(F.one))                          // 1*u = 1*u + 0*u
    assert(mulOne(u))                                 // u = u + 0*u
    uniqueZero(u, sum(u, mul(F.zero, u)))             // 0*u = 0
  }.ensuring(mul(F.zero, u) == zero)
  
  def mulNegOne(u: V) = {
    assert(sumZero(mul(F.neg(F.one), u)))                 //      = u(-1)+0
    assert(sumNeg(u))                                     //      = u(-1)+(u-u)
    assert(mulOne(u))                                     //      = u(-1)+(u(1)-a)
    assert(associativity(mul(F.neg(F.one), u), u, neg(u)))//      = (u(-1)+u(1))-u
    assert(distributivity2(F.neg(F.one), F.one, u))       //      = u(-1+1)-u
    assert(F.sumCommutativity(F.one, F.neg(F.one)))       //      = u(1-1)-u
    assert(F.sumNeg(F.one))                               //      = u 0 -u
    mulZero(u)                                            //      = 0 - u
    assert(commutativity(zero, neg(u)))                   //      = -u + 0
    assert(sumZero(neg(u)))                               //      = -u
  }.ensuring(neg(u) == mul(F.neg(F.one), u))*/
}

case object F2 extends Field[Boolean] {
  def sum(a: Boolean, b: Boolean): Boolean = a != b
  def mul(a: Boolean, b: Boolean): Boolean = a && b
  def neg(a: Boolean): Boolean = a
  def inv(a: Boolean): Boolean = a
  def zero: Boolean = false
  def one: Boolean = true
}

case class FiniteVectorSpace(n : BigInt) extends VectorSpace[Boolean, List[Boolean]] {
  require(n > 0)
  override def F = F2
  override def sum(u: V, v: V): V = {
    require(u.length == n && v.length == n)
    u.zip(v) map {x => F.sum(x._1,x._2)}
  }.ensuring(r => r.length == n)

  def sum2(u: V, v: V): V = {
    u.zip(v) map {x => F.sum(x._1,x._2)}
  }
  override def mul(a: Boolean, u: V): V = {
    require(u.length == n)
    u map {x => F.mul(a,x)}
  }.ensuring(r => r.length == n)
  override def neg(u: V): V = {
    require(u.length == n)
    u map {x => F.neg(x)}
  }.ensuring(r => r.length == n)
  override def zero: V = {
    lemmaTest(n, F.zero)
    List.fill(n)(F.zero)
  }.ensuring(r => r.length == n)

  def lemmaAssociativity(u: V, v: V, w: V): Boolean = {
    require(u.length == n && v.length == n && w.length == n)
    
    def lemmaAssociativityIt(u: V, v: V, w: V): Unit = {
      (u, v, w) match {
        case (Cons(x, xs), Cons(y, ys), Cons(z, zs)) =>
          F.sumAssociativity(x, y, z)
          lemmaAssociativityIt(xs, ys, zs)
        case _ => 
      }
    }.ensuring(sum2(u,sum2(v, w)) == sum2(sum2(u,v),w))
    
    lemmaAssociativityIt(u, v, w)
    sum(u,sum(v,w)) == sum(sum(u,v),w)
  }.holds

  override def associativity(u: V, v: V, w: V) = {
    require(u.length == n && v.length == n && w.length == n)
    super.associativity(u, v, w) because lemmaAssociativity(u, v, w)
  }

  def lemmaCommutativity(u: V, v: V): Boolean = {
    require(u.length == n && v.length == n)

    def lemmaCommutativityIt(u: V,v: V): Unit = {
      (u, v) match {
        case (Cons(x, xs), Cons(y, ys)) =>
          F.sumCommutativity(x,y)
          lemmaCommutativityIt(xs,ys)
        case _ => 
      }
    }.ensuring((u.zip(v) map {x => F.sum(x._1,x._2)}) == (v.zip(u) map {x => F.sum(x._1,x._2)}))
    
    lemmaCommutativityIt(u,v)
    this.sum(u,v) == this.sum(v,u)
  }.holds

  override def commutativity(u: V,v: V) = {
    require(u.length == n && v.length == n)
    super.commutativity(u, v) because lemmaCommutativity(u, v)
  }

  def lemmaSumZero(u: V): Boolean = {
    require(u.length == n)

    def lemmaSumZeroIt(u: V): Unit = {
      u match {
        case Cons(x, xs) =>
          F.sumZero(x)
          lemmaSumZeroIt(xs)
        case Nil() =>
      }
    }.ensuring((u.zip(List.fill(u.length)(F.zero)) map {x => F.sum(x._1,x._2)}) == u)

    lemmaSumZeroIt(u)

    sum(u,zero) == u
  }.holds

  override def sumZero(u: V) = {
    require(u.length == n)
    super.sumZero(u) because lemmaSumZero(u)
  }

  def lemmaMulOne(u: V): Boolean = {
    require(u.length == n)

    def lemmaMulOneIt(v: V): Unit = {
      v match {
        case Cons(x, xs) =>
          F.mulOne(x)
          lemmaMulOneIt(xs)
        case Nil() =>
      }
    }.ensuring(v == (v map {x => F.mul(F.one,x)}))

    lemmaMulOneIt(u)
    u == mul(F.one, u)
  }.holds

  override def mulOne(u: V) = {
    require(u.length == n)
    super.mulOne(u) because lemmaMulOne(u)
  }

  @extern
  def lemmaTest(n: BigInt, obj: Boolean) = {

  }.ensuring(List.fill(n)(obj).length == n)
}

