import stainless.annotation._
import stainless.collection._
import stainless.lang._
import scala.language.postfixOps
import stainless.proof._


// typeclass for Field
abstract class Field[T] {
  def sum(a: T, b: T): T
  
  @law
  def sumCommutativity(a: T, b: T) =
    sum(a,b) == sum(b,a)
  
}

abstract class VectorSpace[T, X]() {
  def F: Field[T]// sorry about the capital, but lower case f would feel very wrong here
  type V = X
  def sum(u: V, v: V): V
  
  @law
  def commutativity(u: V, v: V) = 
    sum(u,v) == sum(v,u)

}

case object F2 extends Field[Boolean] {
  def sum(a: Boolean, b: Boolean): Boolean = a != b
}

case class FiniteVectorSpace(n : BigInt) extends VectorSpace[Boolean, List[Boolean]] {
  require(n > 0)
  override def F = F2
  override def sum(u: V, v: V): V = {
    require(u.length == n && v.length == n)
    u.zip(v) map {x => F.sum(x._1, x._2)}
  }.ensuring(r => r.length == n)

  def lemmaCommutativity(u: V, v: V): Boolean = {
    require(u.length == n && v.length == n)

    def lemmaCommutativityIt(uIt: V, vIt: V): Unit = {
      (uIt, vIt) match {
        case (Cons(x, xs), Cons(y, ys)) =>
          F.sumCommutativity(x,y)
          lemmaCommutativityIt(xs,ys)
        case _ => 
      }
    }.ensuring((uIt.zip(vIt) map {x => F.sum(x._1,x._2)}) == (vIt.zip(uIt) map {x => F.sum(x._1,x._2)}))
    
    lemmaCommutativityIt(u,v)
    this.sum(u,v) == this.sum(v,u)
  }.holds

  override def commutativity(u: V,v: V) = {
    require(u.length == n && v.length == n)
    super.commutativity(u, v) because lemmaCommutativity(u, v)
  }
}

