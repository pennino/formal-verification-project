import stainless.annotation._
import stainless.collection._
import stainless.lang._
import scala.language.postfixOps

object Project {

    // typeclass for Field
    abstract class Scalar[T] {
        def +(b: Scalar[T]): Scalar[T]
        def *(b: Scalar[T]): Scalar[T]
        def unary_- : Scalar[T]
        def unary_! : Scalar[T]
    }

    abstract class Field[T] {
        def zero: Scalar[T]
        def one: Scalar[T]

        @law
        def oneIsNotZero() =
            zero != one
            
        @law
        def sumAssociativity[T](a: Scalar[T], b: Scalar[T], c: Scalar[T]) =
            a + (b + c) == (a + b) + c
            
        @law
        def mulAssociativity(a: Scalar[T], b: Scalar[T], c: Scalar[T]) =
            a * (b * c) == (a * b) * c
            
        @law
        def sumCommutativity(a: Scalar[T], b: Scalar[T]) =
            a + b == b + a
            
        @law
        def mulCommutativity(a: Scalar[T], b: Scalar[T]) =
            a * b == b * a
            
        @law
        def sumZero(a: Scalar[T]) =
            a + zero == a
            
        @law
        def mulOne(a: Scalar[T]) =
            a * one == a
            
        @law
        def sumNeg(a: Scalar[T]) =
            a + (-a) == zero
            
        @law
        def mulInv(a: Scalar[T]) =
            a == zero || a * (!a) == one
            
        @law
        def distributivity(a: Scalar[T], b: Scalar[T], c: Scalar[T]) =
            a * (b + c) == (a * b) + (a * c)


        def uniqueZero(a: Scalar[T], b: Scalar[T]) = {
            require(a + b == b)
            assert(sumZero(a))                  // a = a+0
            assert(sumNeg(b))                   //   = a+(b-b)
            assert(sumAssociativity(a, b, -b))  //   = (a+b)-b
        }.ensuring(a == zero)                   //   = b-b = 0
        
        def uniqueOne(a: Scalar[T], b: Scalar[T]) = {
            require(b != zero && a * b == b)
            assert(mulOne(a))                           // a = a 1
            assert(mulInv(b))                           //   = a (b b^{-1})
            assert(mulAssociativity(a, b, !b))          //   = (a b) b^{-1}
        }.ensuring(a == one)                            //   = b b^{-1} = 1
        
        def mulZero(a: Scalar[T]) = {
            assert(distributivity(a, a, zero))            // aa+a0 = a(a+0)
            assert(sumZero(a))                            //       = aa
            assert(sumCommutativity(a * a, a * zero))     // a0+aa = aa
            uniqueZero(a * zero, a * a)                   // => a0=0
        }.ensuring(a * zero == zero)
        
        def mulNegOne(a: Scalar[T]) = {
            assert(mulCommutativity(-one, a))               //(-1)a = a(-1)
            assert(sumZero(a * (-one)))                     //      = a(-1)+0
            assert(sumNeg(a))                               //      = a(-1)+(a-a)
            assert(mulOne(a))                               //      = a(-1)+(a(1)-a)
            assert(sumAssociativity(a * (-one), a, -a))     //      = (a(-1)+a(1))-a
            assert(distributivity(a, -one, one))            //      = a(-1+1)-a
            assert(sumCommutativity(one, -one))             //      = a(1-1)-a
            assert(sumNeg(one))                             //      = a 0 -a
            mulZero(a)                                      //      = 0 - a
            assert(sumCommutativity(zero, -a))              //      = -a + 0
            assert(sumZero(-a))                             //      = -a
        }.ensuring(-a == (-one) * a)
        
        def zeroProd(a: Scalar[T], b: Scalar[T]) = {
            require(a * b == zero)                          // b != 0 && a != 0 => a^{-1} and b^{-1} exists
            assert(mulInv(a))                               // 1 = a a^{-1}
            assert(mulOne(a * (!a)))                        //   = (a a^{-1}) 1
            assert(mulInv(b))                               //   = (a a^{-1}) (b b^{-1})
            assert(mulCommutativity(a, !a))                 //   = (a^{-1} a) (b b^{-1})
            assert(mulAssociativity((!a) *a, b, !b))        //   = ((a^{-1} a) b) b^{-1}
            assert(mulAssociativity(!a, a, b))              //   = (a^{-1} (ab)) b^{-1}
            mulZero(!a)                                     //   = 0 b^{-1}
            assert(mulCommutativity(zero, !b))              //   = b^{-1} 0
            mulZero(!b)                                     //   = 0
            assert(oneIsNotZero())
        }.ensuring(a == zero || b == zero)
        
        def negZero() = {
            mulNegOne(zero)                         //-0 = (-1) 0
            mulZero(-one)                           //   = 0
        }.ensuring(-zero == zero)
        
        def invOne() = {
            assert(oneIsNotZero())                  //
            assert(mulOne(!one))                    // 1^{-1} = 1^{-1} 1
            assert(mulCommutativity(!one, one))     //        = 1 1^{-1}
            assert(mulInv(one))                     //        = 1
        }.ensuring(!one == one)
        
        def negNeg(a: Scalar[T]) = {
            assert(sumZero(a))                          // a = a + 0
            assert(sumNeg(-a))                          //   = a + (-a -(-a))
            assert(sumAssociativity(a, -a, -(-a)))      //   = (a - a) -(-a)
            assert(sumNeg(a))                           //   = zero -(-a)
            assert(sumCommutativity(zero, -(-a)))       //   = -(-a) + zero
            assert(sumZero(-(-a)))                      //   = -(-a)
        }.ensuring(-(-a) == a)
        
        def invIsNotZero(a: Scalar[T]) = {
            require(a != zero)
            assert(mulInv(a))               // a a^{-1} = 1
            assert(oneIsNotZero())          //         != 0
            mulZero(a)                      //          = a 0
        }.ensuring(!a != zero)              // a^{-1} != 0
        
        def invInv(a: Scalar[T]) = {
            require(a != zero)
            invIsNotZero(a)
            assert(mulOne(a))                           // a = a 1
            assert(mulInv(!a))                          //   = a (a^{-1} (a^{-1})^{-1})
            assert(mulAssociativity(a, !a, !(!a)))      //   = (a a^{-1}) (a^{-1})^{-1}
            assert(mulInv(a))                           //   = 1 (a^{-1})^{-1}
            assert(mulCommutativity(one, !(!a)))        //   = (a^{-1})^{-1} 1
            assert(mulOne(!(!a)))                       //   = (a^{-1})^{-1}
        }.ensuring(a == !(!a))
        
        def mulNeg(a: Scalar[T], b: Scalar[T], c: Scalar[T]) = {
            mulNegOne(a * b)                                    // -(ab) = (-1) (ab)
            assert(mulAssociativity(-one, a, b))                //       = ((-1) a) b
            mulNegOne(a)                                        //       = (-a) b
                                                                // -(ab) = (-1) (ab)
            assert(mulCommutativity(-one, a * b))               //       = (ab)(-1)
            assert(mulAssociativity(a, b, -one))                //       = a (b(-1))
            assert(mulCommutativity(b, -one))                   //       = a ((-1) b)
            mulNegOne(b)                                        //       = a (-b)
        }.ensuring(-(a * b) == (-a) * b && -(a * b) == a * (-b))
    }

    abstract class Vector[T] {
        def +(v: Vector[T]): Vector[T]
        def *(a: Scalar[T]): Vector[T]
        def unary_- : Vector[T]
    }

    abstract class VectorSpace[T] {
        def F: Field[T]

        def zero: Vector[T]

        @law
        def associativity(u: Vector[T], v: Vector[T], w: Vector[T]) =
            u + (v + w) == (u + v) + w
        
        @law
        def commutativity(u: Vector[T], v: Vector[T]) = 
            u + v == v + u
        
        @law
        def sumZero(u: Vector[T]) = 
            u + zero == u
        
        @law
        def sumNeg(u: Vector[T]) =
            u + (-u) == zero
        
        @law
        def compatibility(a: Scalar[T], b: Scalar[T], u: Vector[T]) =
            (u * b) * a == u * (a * b)
        
        @law
        def mulOne(u : Vector[T]) =
            u ==  u * F.one
        
        @law
        def distributivity1(a: Scalar[T], u: Vector[T], v: Vector[T]) =
            (u + v) * a == (u * a) + (v * a)
        
        @law 
        def distributivity2(a: Scalar[T], b: Scalar[T], u: Vector[T]) =
            u * (a + b) == (u * a) + (u * b)
        
        def mulNegOne(u: Vector[T]) = {
            
        }.ensuring(-u == u * (-F.one))
    }

    case class BooleanScalar(val a: Boolean) extends Scalar[Boolean] {
        def +(b: BooleanScalar): BooleanScalar = BooleanScalar(a != b.a)
        def *(b: BooleanScalar): BooleanScalar = BooleanScalar(a && b.a)
        def unary_- : BooleanScalar = BooleanScalar(a)
        def unary_! : BooleanScalar = BooleanScalar(a)
    }

    case object F2 extends Field[Boolean] {
        def zero: BooleanScalar = BooleanScalar(false)
    }

    case class BooleanVector(val u: List[BooleanScalar]) extends Vector[BooleanScalar] {
        def +(v: BooleanVector): BooleanVector = {
            require(u.length == v.u.length)
            BooleanVector(u.zip(v.u).map(x => x._1 + x._2))
        }.ensuring(r => r.u.length == u.length)
        def *(a: BooleanScalar): BooleanVector = {
            BooleanVector(u.map(x => x * a))
        }.ensuring(r => r.u.length == u.length)   
        def unary_- : BooleanVector = {
            BooleanVector(u.map(x => -x))
        }.ensuring(r => r.u.length == u.length)
    }

    case class BooleanVectorSpace(n: BigInt) extends VectorSpace[Boolean] {
        def F = F2

        def zero: BooleanVector = {
            BooleanVector(List.fill(n)(F.zero))
        }.ensuring(r => r.u.length == n)
    }
}