import stainless.annotation._
import stainless.collection._
import stainless.lang._
import scala.language.postfixOps
import stainless.proof._

abstract class VectorSpace[T]() {
  def sum2(u: List[T], v: List[T]): List[T]
}

case object FiniteVectorSpace extends VectorSpace[Boolean] {
  override def sum2(u: List[Boolean], v: List[Boolean]): List[Boolean] = {
    require(u.length == v.length)
    Nil()
  }
}

  /*override def sum2(u: List[Boolean], v: List[Boolean]): List[Boolean] = {
    require(u.length == v.length)
    u.zip(v) map {x => x._1 != x._2}
  }.ensuring(r => r.length == u.length)*/

