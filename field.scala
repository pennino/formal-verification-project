import stainless.annotation._
import stainless.collection._
import stainless.lang._
import scala.language.postfixOps
import stainless.proof._

case object F2 {
  //Field operations
  def sum(a: Boolean, b: Boolean): Boolean = a != b
  def mul(a: Boolean, b: Boolean): Boolean = a && b
  def neg(a: Boolean): Boolean = a
  def inv(a: Boolean): Boolean = a
  def zeroF: Boolean = false
  def oneF: Boolean = true

  def sum(as: List[Boolean]): Boolean = as match {
    case Cons(x, xs) => sum(x, sum(xs))
    case Nil() => zeroF
  }
  
  def mul(as: List[Boolean]): Boolean = as match {
    case Cons(x, xs) => xs.foldLeft(x)(mul)
    case Nil() => oneF
  }
  
  // Would be good to have them define as sum(as: T*) too, not sure if possible in stainless
  
  def foldMul(a: Boolean, n: BigInt): Boolean = {
    decreases(if (n < 0) -n+1 else n)
    if (n < 0) foldMul(inv(a), -n)
    else if (n == 0) oneF
    else if (n%2 == 0) foldMul(mul(a, a), n/2)
    else mul(foldMul(mul(a, a), (n-1)/2), a)
  }
  
  def foldSum(a: Boolean, n: BigInt): Boolean = {
    decreases(if (n < 0) -n+1 else n)
    if (n < 0) foldSum(neg(a), -n)
    else if (n == 0) zeroF
    else if (n%2 == 0) foldSum(sum(a, a), n/2)
    else sum(foldSum(sum(a, a), (n-1)/2), a)
  }
  
  //Field axioms
  def oneIsNotZero() = {
    zeroF != oneF
  }.holds
    
  
  def sumAssociativity(a: Boolean, b: Boolean, c: Boolean) = {
    sum(a ,sum(b, c)) == sum(sum(a, b), c)
  }.holds
    
  
  def mulAssociativity(a: Boolean, b: Boolean, c: Boolean) = {
    mul(a, mul(b, c)) == mul(mul(a, b), c)
  }.holds
    
  def sumCommutativity(a: Boolean, b: Boolean) = {
    sum(a, b) == sum(b, a)
  }.holds
  
  def mulCommutativity(a: Boolean, b: Boolean) = {
    mul(a, b) == mul(b, a)
  }.holds
  
  def sumZero(a: Boolean) = {
    sum(a, zeroF) == a
  }.holds
  
  def mulOne(a: Boolean) = {
    mul(a, oneF) == a
  }.holds
     
  def sumNeg(a: Boolean) = {
    sum(a, neg(a)) == zeroF
  }.holds
  
  def mulInv(a: Boolean) = {
    a == zeroF || mul(a, inv(a)) == oneF
  }.holds
  
  def distributivity(a: Boolean, b: Boolean, c: Boolean) = {
    mul(a, sum(b, c)) == sum(mul(a, b), mul(a, c))
  }.holds

  def uniqueZero(a: Boolean, b: Boolean) = {
    require(sum(a, b) == b)
    assert(sumZero(a))                          // a = a+0
    assert(sumNeg(b))                           //   = a+(b-b)
    assert(sumAssociativity(a, b, neg(b)))      //   = (a+b)-b
  }.ensuring(a == zeroF)                        //   = b-b = 0
  
  def uniqueOne(a: Boolean, b: Boolean) = {
    require(b != zeroF && mul(a, b) == b)
    assert(mulOne(a))                           // a = a 1
    assert(mulInv(b))                           //   = a (b b^{-1})
    assert(mulAssociativity(a, b, inv(b)))      //   = (a b) b^{-1}
  }.ensuring(a == oneF)                         //   = b b^{-1} = 1
  
  def mulZero(a: Boolean) = {
    assert(distributivity(a, a, zeroF))               // aa+a0 = a(a+0)
    assert(sumZero(a))                                //       = aa
    assert(sumCommutativity(mul(a, a), mul(a, zeroF)))// a0+aa = aa
    uniqueZero(mul(a, zeroF), mul(a, a))              // => a0=0
  }.ensuring(mul(a, zeroF) == zeroF)
  
  def mulNegOne(a: Boolean) = {
    assert(mulCommutativity(neg(oneF), a))                //(-1)a = a(-1)
    assert(sumZero(mul(a, neg(oneF))))                    //      = a(-1)+0
    assert(sumNeg(a))                                     //      = a(-1)+(a-a)
    assert(mulOne(a))                                     //      = a(-1)+(a(1)-a)
    assert(sumAssociativity(mul(a, neg(oneF)), a, neg(a)))//      = (a(-1)+a(1))-a
    assert(distributivity(a, neg(oneF), oneF))            //      = a(-1+1)-a
    assert(sumCommutativity(oneF, neg(oneF)))             //      = a(1-1)-a
    assert(sumNeg(oneF))                                  //      = a 0 -a
    mulZero(a)                                            //      = 0 - a
    assert(sumCommutativity(zeroF, neg(a)))               //      = -a + 0
    assert(sumZero(neg(a)))                               //      = -a
  }.ensuring(neg(a) == mul(neg(oneF), a))
  
  def zeroProd(a: Boolean, b: Boolean) = {
    require(mul(a, b) == zeroF)                         // b != 0 && a != 0 => a^{-1} and b^{-1} exists
    assert(mulInv(a))                                   // 1 = a a^{-1}
    assert(mulOne(mul(a, inv(a))))                      //   = (a a^{-1}) 1
    assert(mulInv(b))                                   //   = (a a^{-1}) (b b^{-1})
    assert(mulCommutativity(a, inv(a)))                 //   = (a^{-1} a) (b b^{-1})
    assert(mulAssociativity(mul(inv(a), a), b, inv(b))) //   = ((a^{-1} a) b) b^{-1}
    assert(mulAssociativity(inv(a), a, b))              //   = (a^{-1} (ab)) b^{-1}
    mulZero(inv(a))                                     //   = 0 b^{-1}
    assert(mulCommutativity(zeroF, inv(b)))             //   = b^{-1} 0
    mulZero(inv(b))                                     //   = 0
    assert(oneIsNotZero())
  }.ensuring(a == zeroF || b == zeroF)
  
  def negZero() = {
    mulNegOne(zeroF)                          //-0 = (-1) 0
    mulZero(neg(oneF))                        //   = 0
  }.ensuring(neg(zeroF) == zeroF)
  
  def invOne() = {
    assert(oneIsNotZero())                    //
    assert(mulOne(inv(oneF)))                 // 1^{-1} = 1^{-1} 1
    assert(mulCommutativity(inv(oneF), oneF)) //        = 1 1^{-1}
    assert(mulInv(oneF))                      //        = 1
  }.ensuring(inv(oneF) == oneF)
  
  def negNeg(a: Boolean) = {
    assert(sumZero(a))                              // a = a + 0
    assert(sumNeg(neg(a)))                          //   = a + (-a -(-a))
    assert(sumAssociativity(a, neg(a), neg(neg(a))))//   = (a - a) -(-a)
    assert(sumNeg(a))                               //   = zero -(-a)
    assert(sumCommutativity(zeroF, neg(neg(a))))    //   = -(-a) + zero
    assert(sumZero(neg(neg(a))))                    //   = -(-a)
  }.ensuring(neg(neg(a)) == a)
  
  def InvIsNotZero(a: Boolean) = {
    require(a != zeroF)
    assert(mulInv(a))               // a a^{-1} = 1
    assert(oneIsNotZero())          //         != 0
    mulZero(a)                      //          = a 0
  }.ensuring(inv(a) != zeroF)       // a^{-1} != 0
  
  def invInv(a: Boolean) = {
    require(a != zeroF)
    InvIsNotZero(a)
    assert(mulOne(a))                               // a = a 1
    assert(mulInv(inv(a)))                          //   = a (a^{-1} (a^{-1})^{-1})
    assert(mulAssociativity(a, inv(a), inv(inv(a))))//   = (a a^{-1}) (a^{-1})^{-1}
    assert(mulInv(a))                               //   = 1 (a^{-1})^{-1}
    assert(mulCommutativity(oneF, inv(inv(a))))     //   = (a^{-1})^{-1} 1
    assert(mulOne(inv(inv(a))))                     //   = (a^{-1})^{-1}
  }.ensuring(a == inv(inv(a)))
  
  def mulNeg(a: Boolean, b: Boolean, c: Boolean) = {
    mulNegOne(mul(a, b))                             // -(ab) = (-1) (ab)
    assert(mulAssociativity(neg(oneF), a, b))        //       = ((-1) a) b
    mulNegOne(a)                                     //       = (-a) b
                                                     // -(ab) = (-1) (ab)
    assert(mulCommutativity(neg(oneF), mul(a, b)))   //       = (ab)(-1)
    assert(mulAssociativity(a, b, neg(oneF)))        //       = a (b(-1))
    assert(mulCommutativity(b, neg(oneF)))           //       = a ((-1) b)
    mulNegOne(b)                                     //       = a (-b)
  }.ensuring(neg(mul(a, b)) == mul(neg(a), b) && neg(mul(a, b)) == mul(a, neg(b)))
}

case class FiniteVectorSpace(n: BigInt) {
  require(n > 0)

  def F = F2

  //Vector space operations
  override def sum(u: List[Boolean], v: List[Boolean]): List[Boolean] = {
    require(u.length == n && v.length == n)
    u.zip(v) map {x => F.sum(x._1, x._2)}
  }.ensuring(r => r.length == n)

  def sum2(u: List[Boolean], v: List[Boolean]): List[Boolean] = {
    u.zip(v) map {x => F.sum(x._1, x._2)}
  }

  def mul(a: Boolean, u: List[Boolean]): List[Boolean] = {
    require(u.length == n)
    u map {x => F.mul(a, x)}
  }.ensuring(r => r.length == n)

  def mul2(a: Boolean, u: List[Boolean]): List[Boolean] = {
    u map {x => F.mul(a, x)}
  }

  def neg(u: List[Boolean]): List[Boolean] = {
    require(u.length == n)
    u map {x => F.neg(x)}
  }.ensuring(r => r.length == n)

  def zero: List[Boolean] = {
    List.fill(n)(F.zeroF)
  }.ensuring(r => r.length == n)

  //Vector space axioms
  def associativity(u: List[Boolean], v: List[Boolean], w: List[Boolean]) = {
    require(u.length == n && v.length == n && w.length == n)
    lemmaAssociativity(u, v, w)
    sum(u, sum(v, w)) == sum(sum(u, v), w)
  }.holds
  
  def commutativity(u: List[Boolean], v: List[Boolean]) = {
    require(u.length == n && v.length == n)
    lemmaCommutativity(u, v)
    sum(u, v) == sum(v, u)
  }.holds
  
  def sumZero(u: List[Boolean]) = {
    require(u.length == n)
    lemmaSumZero(u)
    sum(u, zero) == u
  }.holds
  
  def sumNeg(u: List[Boolean]) = {
    require(u.length == n)
    lemmaSumNeg(u)
    sum(u, neg(u)) == zero
  }.holds
  
  def compatibility(a: Boolean, b: Boolean, u: List[Boolean]) = {
    require(u.length == n)
    lemmaCompatibility(a, b, u)
    mul(a, mul(b, u)) == mul(F.mul(a, b), u)
  }.holds
  
  def mulOne(u: List[Boolean]) = {
    require(u.length == n)
    lemmaMulOne(u)
    u == mul(F.oneF, u)
  }.holds
  
  def distributivity1(a: Boolean, u: List[Boolean], v: List[Boolean]) = {
    require(u.length == n && v.length == n)
    lemmaDistributivity1(a, u, v)
    mul(a, sum(u, v)) == sum(mul(a, u), mul(a, v))
  }.holds
  
  def distributivity2(a: Boolean, b: Boolean, u: List[Boolean]) = {
    require(u.length == n)
    lemmaDistributivity2(a, b, u)
    mul(F.sum(a, b), u) == sum(mul(a, u), mul(b, u))
  }.holds

  def uniqueZero(u: List[Boolean], v: List[Boolean]) = {
    require(u.length == n && v.length == n && sum(v, u) == u)
    assert(sumZero(v))                        // v = v+0
    assert(sumNeg(u))                         //   = v+(u-u)
    assert(associativity(v, u, neg(u)))       //   = (v+u)-u
  }.ensuring(v == zero)                       //   = u-u = 0

  def mulZero(u: List[Boolean]) = {
    require(u.length == n)
    assert(distributivity2(F.zeroF, F.oneF, u)) // (1+0)*u = 1*u + 0*u
    assert(F.sumCommutativity(F.zeroF, F.oneF))
    assert(F.sumZero(F.oneF))                   // 1*u = 1*u + 0*u
    assert(mulOne(u))                           // u = u + 0*u
    assert(mul(F.zeroF, u).length == n)
    uniqueZero(u, mul(F.zeroF, u))              // 0*u = 0
  }.ensuring(mul(F.zeroF, u) == zero)
  
  def mulNegOne(u: List[Boolean]) = {
    require(u.length == n)
    assert(sumZero(mul(F.neg(F.oneF), u)))                  //      = u(-1)+0
    assert(sumNeg(u))                                       //      = u(-1)+(u-u)
    assert(mulOne(u))                                       //      = u(-1)+(u(1)-a)
    assert(associativity(mul(F.neg(F.oneF), u), u, neg(u))) //      = (u(-1)+u(1))-u
    assert(distributivity2(F.neg(F.oneF), F.oneF, u))       //      = u(-1+1)-u
    assert(F.sumCommutativity(F.oneF, F.neg(F.oneF)))       //      = u(1-1)-u
    assert(F.sumNeg(F.oneF))                                //      = u 0 -u
    mulZero(u)                                              //      = 0 - u
    assert(commutativity(zero, neg(u)))                     //      = -u + 0
    assert(sumZero(neg(u)))                                 //      = -u
  }.ensuring(neg(u) == mul(F.neg(F.oneF), u))


  def lemmaAssociativity(u: List[Boolean], v: List[Boolean], w: List[Boolean]): Boolean = {
    require(u.length == n && v.length == n && w.length == n)
    
    def lemmaAssociativityIt(u: List[Boolean], v: List[Boolean], w: List[Boolean]): Unit = {
      (u, v, w) match {
        case (Cons(x, xs), Cons(y, ys), Cons(z, zs)) =>
          F.sumAssociativity(x, y, z)
          lemmaAssociativityIt(xs, ys, zs)
        case _ => 
      }
    }.ensuring(sum2(u,sum2(v, w)) == sum2(sum2(u,v),w))
    
    lemmaAssociativityIt(u, v, w)
    sum(u, sum(v, w)) == sum(sum(u, v), w)
  }.holds

  def lemmaCommutativity(u: List[Boolean], v: List[Boolean]): Boolean = {
    require(u.length == n && v.length == n)

    def lemmaCommutativityIt(u: List[Boolean],v: List[Boolean]): Unit = {
      (u, v) match {
        case (Cons(x, xs), Cons(y, ys)) =>
          F.sumCommutativity(x, y)
          lemmaCommutativityIt(xs, ys)
        case _ => 
      }
    }.ensuring((u.zip(v) map {x => F.sum(x._1, x._2)}) == (v.zip(u) map {x => F.sum(x._1, x._2)}))
    
    lemmaCommutativityIt(u, v)
    sum(u, v) == sum(v, u)
  }.holds

  def lemmaSumZero(u: List[Boolean]): Boolean = {
    require(u.length == n)

    def lemmaSumZeroIt(u: List[Boolean]): Unit = {
      u match {
        case Cons(x, xs) =>
          F.sumZero(x)
          lemmaSumZeroIt(xs)
        case Nil() =>
      }
    }.ensuring((u.zip(List.fill(u.length)(F.zeroF)) map {x => F.sum(x._1, x._2)}) == u)

    lemmaSumZeroIt(u)
    sum(u, zero) == u
  }.holds

  def lemmaSumNeg(u: List[Boolean]): Boolean = {
    require(u.length == n)

    def lemmaSumNegIt(u: List[Boolean]): Unit = {
      u match {
        case Cons(x, xs) =>
          F.sumNeg(x)
          lemmaSumNegIt(xs)
        case Nil() =>
      }
    }.ensuring((u.zip(u.map(x => F.neg(x))) map {x => F.sum(x._1, x._2)}) == List.fill(u.length)(F.zeroF))

    lemmaSumNegIt(u)
    sum(u, neg(u)) == zero
  }.holds

  def lemmaCompatibility(a: Boolean, b: Boolean, u: List[Boolean]): Boolean = {
    require(u.length == n)

    def lemmaCompatibilityIt(u: List[Boolean]): Unit = {
      u match {
        case Cons(x, xs) =>
          F.mulAssociativity(a, b, x)
          lemmaCompatibilityIt(xs)
        case Nil() =>
      }
    }.ensuring(mul2(a, mul2(b, u)) == mul2(F.mul(a, b), u))

    lemmaCompatibilityIt(u)
    mul(a, mul(b, u)) == mul(F.mul(a, b), u)
  }.holds

  def lemmaMulOne(u: List[Boolean]): Boolean = {
    require(u.length == n)

    def lemmaMulOneIt(v: List[Boolean]): Unit = {
      v match {
        case Cons(x, xs) =>
          F.mulOne(x)
          lemmaMulOneIt(xs)
        case Nil() =>
      }
    }.ensuring(v == (v map {x => F.mul(F.oneF, x)}))

    lemmaMulOneIt(u)
    u == mul(F.oneF, u)
  }.holds

  def lemmaDistributivity1(a: Boolean, u: List[Boolean], v: List[Boolean]): Boolean = {
    require(u.length == n && v.length == n)

    def lemmaDistributivity1It(a: Boolean, u: List[Boolean], v: List[Boolean]): Unit = {
      (u, v) match {
        case (Cons(x, xs), Cons(y, ys)) =>
          F.distributivity(a, x, y)
          lemmaDistributivity1It(a, xs, ys)
        case _ =>
      }
    }.ensuring(mul2(a, sum2(u, v)) == sum2(mul2(a, u), mul2(a, v)))

    lemmaDistributivity1It(a, u, v)
    mul(a, sum(u, v)) == sum(mul(a, u), mul(a, v))
  }.holds

  def lemmaDistributivity2(a: Boolean, b: Boolean, u: List[Boolean]): Boolean = {
    require(u.length == n)

    def lemmaDistributivity2It(a: Boolean, b: Boolean, u: List[Boolean]): Unit = {
      u match {
        case Cons(x, xs) =>
          F.distributivity(a, b, x)
          lemmaDistributivity2It(a, b, xs)
        case _ =>
      }
    }.ensuring(mul2(F.sum(a, b), u) == sum2(mul2(a, u), mul2(b, u)))

    lemmaDistributivity2It(a, b, u)
    mul(F.sum(a, b), u) == sum(mul(a, u), mul(b, u))
  }.holds
  
}

case class LinearOperatorSpace(n: BigInt, m: BigInt) {
  require(n > 0 && m > 0 && n < m)

  def Vn = FiniteVectorSpace(n)
  def Vm = FiniteVectorSpace(m)
  
  require(Vn.F == Vm.F)
  def F = Vn.F
  val maxRank = if(n < m) n else m
  
  def rightRawLength(x: List[Boolean]): Boolean = {
    x.length == n
  }
  
  def rightDimension(mat: List[List[Boolean]]): Boolean = {
    mat.length == m && mat.forall(rightRawLength)
  }

  def sumIt(a: List[List[Boolean]], b: List[List[Boolean]]): List[List[Boolean]] = {
    require(a.length == b.length && a.forall(rightRawLength) && b.forall(rightRawLength))
    (a, b) match {
      case (Cons(x, xs), Cons(y, ys)) => Cons(Vn.sum(x, y), sumIt(xs, ys))
      case _ => a
    }
  }.ensuring(r => r.length == a.length && r.forall(rightRawLength))

  def sum(a: List[List[Boolean]], b: List[List[Boolean]]): List[List[Boolean]] = {
    require(a.length == m && b.length == m && a.forall(rightRawLength) && b.forall(rightRawLength))
    sumIt(a, b)
  }.ensuring(r => rightDimension(r))

  def mulIt(a: Boolean, b: List[List[Boolean]]): List[List[Boolean]] = {
    require(b.forall(rightRawLength))
    b match {
      case Cons(x, xs) => Cons(Vn.mul(a, x), mulIt(a, xs))
      case Nil() => b
    }
  }.ensuring(r => r.length == b.length && r.forall(rightRawLength))

  def mul(a: Boolean, b: List[List[Boolean]]): List[List[Boolean]] = {
    require(rightDimension(b))
    mulIt(a, b)
  }.ensuring(r => rightDimension(r))

  def product(a: List[Boolean], b: List[Boolean]): List[Boolean] = {
    require(a.length == b.length)
    a.zip(b).map(c => Vn.F.mul(c._1, c._2))
  }.ensuring(r => r.length == a.length)

  def scalarProduct(a: List[Boolean], b: List[Boolean]): Boolean = {
    require(a.length == b.length)
    Vn.F.sum(a.zip(b).map(c => Vn.F.mul(c._1, c._2)))
  }

  def frontApplyIt(a: List[Boolean], b: List[List[Boolean]]): List[Boolean] = {
    require(a.length == n && b.forall(rightRawLength))
    b match {
      case Cons(x, xs) => Cons(scalarProduct(a, x), frontApplyIt(a, xs))
      case Nil() => Nil[Boolean]()
    }
  }.ensuring(r => r.length == b.length)

  def frontApply(a: List[Boolean], b: List[List[Boolean]]): List[Boolean] = {
    require(a.length == n && b.length == m && b.forall(rightRawLength))
    frontApplyIt(a, b)
  }.ensuring(r => r.length == m)
  
  def innerIt(i: BigInt, xs: List[Boolean]): Boolean = xs match {
    case Nil() => true
    case Cons(y, ys) => (if (i == 0) y == Vn.F.oneF else y == Vn.F.zeroF) && innerIt(i-1, ys)
  }
  
  def outerIt(i: BigInt, mat: List[List[Boolean]]):Boolean = mat match {
    case ls if ls.length <= m-n => true
    case Cons(l, ls) => if (innerIt(i, l)) outerIt(i+1, ls)
      else false
  }

  def isStandardForm(mat: List[List[Boolean]]): Boolean = {
    require(rightDimension(mat))

    outerIt(0, mat)
  }

  def get[T](a: List[T], i: BigInt): T = {
    require(i >= 0 && i < a.length)
    a match {
      case Cons(x, xs) => if (i == 0) x else get(xs, i - 1)
    }
  }

  def rowSwitchIt(mat: List[List[Boolean]], rowI: List[Boolean], rowJ: List[Boolean], i: BigInt, j: BigInt): List[List[Boolean]] = {
    require(mat.forall(rightRawLength) && rightRawLength(rowI) && rightRawLength(rowJ))

    mat match {
      case Cons(x, xs) => 
        if (i == 0) Cons(rowI, rowSwitchIt(xs, rowI, rowJ, i-1, j-1))
        else if (j == 0) Cons(rowJ, rowSwitchIt(xs, rowI, rowJ, i-1, j-1))
        else Cons(x, rowSwitchIt(xs, rowI, rowJ, i-1, j-1))
      case Nil() => Nil[List[Boolean]]()
    }

  }.ensuring(r => r.length == mat.length && r.forall(rightRawLength))

  def rowSwitch(mat: List[List[Boolean]], i: BigInt, j: BigInt): List[List[Boolean]] = {
    require(rightDimension(mat) && 0 <= i && i < m && 0 <= j && j < m)
    getReturnCorrectLength(mat, i)
    getReturnCorrectLength(mat, j)
    rowSwitchIt(mat, get(mat, i), get(mat, j), i, j)
  }.ensuring(r => rightDimension(r))
  
  def rowMulIt(mat: List[List[Boolean]], i: BigInt, k: Boolean): List[List[Boolean]] = {
    require(i < mat.length && mat.forall(rightRawLength) && k != F.zeroF)
    
    mat match {
      case Cons(x, xs) => if (i == 0) Cons(Vn.mul(k, x), rowMulIt(xs, i-1, k)) else Cons(x, rowMulIt(xs, i-1, k))
      case Nil() => Nil[List[Boolean]]()
    }

  }.ensuring(r => r.length == mat.length && r.forall(rightRawLength))
  
  //On F2, this is really useless, but in a more general setting it would be usefull
  def rowMul(mat: List[List[Boolean]], i: BigInt, k: Boolean): List[List[Boolean]] = {
    require(rightDimension(mat) && k != F.zeroF && 0 <= i && i < m)
    rowMulIt(mat, i, k)
  }.ensuring(r => rightDimension(r))
  
  def getReturnCorrectLength(mat: List[List[Boolean]], i: BigInt): Unit = {
    require(mat.forall(rightRawLength) && i >= 0 && i < mat.length)

    mat match {
      case Cons(x, xs) => if (i > 0) getReturnCorrectLength(xs, i - 1)
      case Nil() =>
    }

  }.ensuring(get(mat, i).length == n)

  def rowAddIt(mat: List[List[Boolean]], i: BigInt, rowJ: List[Boolean], k: Boolean): List[List[Boolean]] = {
    require(i < mat.length && mat.forall(rightRawLength) && rightRawLength(rowJ))
    mat match {
      case Cons(x, xs) => 
        if (i == 0) Cons(Vn.sum(Vn.mul(k, rowJ), x), rowAddIt(xs, i-1, rowJ, k)) 
        else Cons(x, rowAddIt(xs, i-1, rowJ, k))
      case Nil() => Nil[List[Boolean]]()
    }
  }.ensuring(r => r.length == mat.length && r.forall(rightRawLength))
  
  def rowAdd(mat: List[List[Boolean]], i: BigInt, j: BigInt, k: Boolean): List[List[Boolean]] = {
    require(rightDimension(mat) && 0 <= i && i < m && 0 <= j && j < m && i != j)
    getReturnCorrectLength(mat, i)
    rowAddIt(mat, j, get(mat, i), k)
  }.ensuring(r => rightDimension(r))

  def lemmaRowSwitchPreservesKerIt(a: List[Boolean], b: List[List[Boolean]], rowI: List[Boolean], rowJ: List[Boolean], i: BigInt, j: BigInt): Unit = {
    require(b.forall(rightRawLength) && rightRawLength(rowI) && rightRawLength(rowJ) && rightRawLength(a) && frontApplyIt(a, b) == List.fill(b.length)(Vm.F.zeroF)
    && scalarProduct(a, rowI) == Vm.F.zeroF && scalarProduct(a, rowJ) == Vm.F.zeroF)
    
    b match {
      case Cons(x, xs) => 
        if (i == 0) lemmaRowSwitchPreservesKerIt(a, xs, rowI, rowJ, i-1, j-1)
        else if (j == 0) lemmaRowSwitchPreservesKerIt(a, xs, rowI, rowJ, i-1, j-1)
        else lemmaRowSwitchPreservesKerIt(a, xs, rowI, rowJ, i-1, j-1)
      case Nil() => 
    }

  }.ensuring(frontApplyIt(a,rowSwitchIt(b, rowI, rowJ, i, j)) == List.fill(b.length)(Vm.F.zeroF))

  def getIfFrontApplyZeroScalarProductZeroIt(a: List[Boolean], b: List[List[Boolean]], i: BigInt): Unit = {
    require(i >= 0 && i < b.length && b.forall(rightRawLength) && rightRawLength(a) && frontApplyIt(a, b) == List.fill(b.length)(Vm.F.zeroF))
    getReturnCorrectLength(b, i)
    b match {
      case Cons(x, xs) => if (i > 0) getIfFrontApplyZeroScalarProductZeroIt(a, xs, i-1)
      case Nil() =>
    }

  }.ensuring(scalarProduct(a, get(b, i)) == Vm.F.zeroF)

  def getIfFrontApplyZeroScalarProductZero(a: List[Boolean], b: List[List[Boolean]], i: BigInt): Unit = {
    require(i >= 0 && i < b.length && rightDimension(b) && rightRawLength(a) && frontApply(a, b) == Vm.zero)
    getReturnCorrectLength(b, i)
    getIfFrontApplyZeroScalarProductZeroIt(a, b, i)
  }.ensuring(scalarProduct(a, get(b, i)) == Vm.F.zeroF)

  def lemmaRowSwitchPreservesKer(a: List[Boolean], b: List[List[Boolean]], i: BigInt, j: BigInt): Unit = {
    require(rightDimension(b) && rightRawLength(a) && 0 <= i && i < m && 0 <= j && j < m && frontApply(a, b) == Vm.zero)
    getReturnCorrectLength(b, i)
    getReturnCorrectLength(b, j)
    getIfFrontApplyZeroScalarProductZero(a, b, i)
    getIfFrontApplyZeroScalarProductZero(a, b, j)
    lemmaRowSwitchPreservesKerIt(a, b, get(b, i), get(b, j), i, j)
  }.ensuring(frontApply(a, rowSwitch(b, i, j)) == Vm.zero)
  
  def lemmaScalarProductCompatibility(a: List[Boolean], b: List[Boolean], c: Boolean): Unit = {
    require(a.length == b.length)

    (a, b) match {
      case (Cons(x, xs), Cons(y, ys)) =>
        //assert(Vn.F.mul(Vn.F.sum(x, y), z) == Vm.F.sum(Vn.F.mul(x, z), Vn.F.mul(y, z)))
        lemmaScalarProductCompatibility(xs, ys, c)
      case _ =>
    }

  }.ensuring(scalarProduct(a, Vn.mul2(c, b)) == Vn.F.mul(c, scalarProduct(a, b)))

  def lemmaRowMulPreservesKerIt(a: List[Boolean], b: List[List[Boolean]], i: BigInt, k: Boolean): Unit = {
    require(b.forall(rightRawLength) && rightRawLength(a) && k != F.zeroF && i < b.length && frontApplyIt(a, b) == List.fill(b.length)(Vm.F.zeroF))
    b match {
      case Cons(x, xs) => 
        if (i == 0) {
          lemmaScalarProductCompatibility(a, x, k)
          lemmaRowMulPreservesKerIt(a, xs, i-1, k)
        } 
        else lemmaRowMulPreservesKerIt(a, xs, i-1, k)
      case Nil() =>
    }
  }.ensuring(frontApplyIt(a,rowMulIt(b, i, k)) == List.fill(b.length)(Vm.F.zeroF))

  def lemmaRowMulPreservesKer(a: List[Boolean], b: List[List[Boolean]], i: BigInt, k: Boolean): Unit = {
    require(rightDimension(b) && rightRawLength(a) && k != F.zeroF && 0 <= i && i < m && frontApply(a, b) == Vm.zero)
    lemmaRowMulPreservesKerIt(a, b, i, k)
  }.ensuring(frontApply(a,rowMul(b, i, k)) == Vm.zero)

  def lemmaRowAddPreservesKerIt(a: List[Boolean], b: List[List[Boolean]], i: BigInt, rowJ: List[Boolean], k: Boolean): Unit = {
    require(b.forall(rightRawLength) && rightRawLength(a) && i < b.length && rightRawLength(rowJ) && frontApplyIt(a, b) == List.fill(b.length)(Vm.F.zeroF) && scalarProduct(a, rowJ) == Vm.F.zeroF)
    b match {
      case Cons(x, xs) => 
        if (i == 0) {
          lemmaScalarProductDistributivity1(a, Vn.mul(k, rowJ), x)
          lemmaScalarProductCompatibility(a, rowJ, k)
          lemmaRowAddPreservesKerIt(a, xs, i-1, rowJ, k)
        }
        else lemmaRowAddPreservesKerIt(a, xs, i-1, rowJ, k)
      case Nil() =>
    }
  }.ensuring(frontApplyIt(a,rowAddIt(b, i, rowJ, k)) == List.fill(b.length)(Vm.F.zeroF))

  def lemmaRowAddPreservesKer(a: List[Boolean], b: List[List[Boolean]], i: BigInt, j: BigInt, k: Boolean): Unit = {
    require(rightDimension(b) && rightRawLength(a) && 0 <= i && i < m && 0 <= j && j < m && i != j && frontApply(a, b) == Vm.zero)
    getReturnCorrectLength(b, i)
    getIfFrontApplyZeroScalarProductZero(a, b, i)
    lemmaRowAddPreservesKerIt(a, b, j, get(b, i), k)
  }.ensuring(frontApply(a,rowAdd(b, i, j, k)) == Vm.zero)
  
  def gaussEliminationItForwardInner(a: List[List[Boolean]], i: BigInt, k: BigInt, j: BigInt):List[List[Boolean]] = {
    require(rightDimension(a) && 0 <= i && i < m && 0 <= j && j < n && 0 <= k && k <= m)
    decreases(m-k)
    if(k == m) a
    else if(k == i) a
    else {
      getReturnCorrectLength(a,k)
      if(get(get(a,k),j) == F.zeroF) gaussEliminationItForwardInner(a, i, k+1, j)
      else gaussEliminationItForwardInner(rowAdd(a, i, k, F.neg(get(get(a,k),j))), i, k+1, j)
    }
  }.ensuring(r => rightDimension(r))
  
  def gaussEliminationItForwardFindNonZero(a: List[List[Boolean]], i: BigInt, j: BigInt):BigInt = {
    require(rightDimension(a) && 0 <= i && i <= m && 0 <= j && j < n)
    decreases(m-i)
    if(i == m) BigInt(-1)
    else {
      getReturnCorrectLength(a,i)
      if(get(get(a,i),j) == F.zeroF) gaussEliminationItForwardFindNonZero(a,i+1,j)
      else i
    }
  }.ensuring(r => r == -1 || (0 <= r && r < m && {getReturnCorrectLength(a,r);get(get(a,r),j) != F.zeroF}))
  
  def gaussEliminationItForwardOuter(a: List[List[Boolean]], i: BigInt, j: BigInt):List[List[Boolean]] = {
    require(rightDimension(a) && 0 <= i && i <= m && 0 <= j && j <= n)
    decreases(m-i+m-j)
    if(i == m || j == n) a
    else 
    {
      var na = a
      var ni = i+1
      var nj = j+1
      getReturnCorrectLength(a,i)
      if(get(get(a,i),j) == F.oneF) {na=gaussEliminationItForwardInner(a,i,0,j)}
      else if(get(get(a,i),j) != F.zeroF) {na=gaussEliminationItForwardInner(rowMul(a, i, F.inv(get(get(a,i),j))),i,0,j)}
      else {
        val nonZeroidx = gaussEliminationItForwardFindNonZero(a,i+1,j)
        if(nonZeroidx == -1) {ni=i}
        //Forced to repeat code from above because of measure decreases
        else if(get(get(a,i),j) == F.oneF) {na=gaussEliminationItForwardInner(rowSwitch(a, i, nonZeroidx),i,0,j)}
        else {
          val naTemp = rowSwitch(a, i, nonZeroidx)
          getReturnCorrectLength(a,nonZeroidx)
          F.InvIsNotZero(get(get(a,nonZeroidx),j))
          na = gaussEliminationItForwardInner(rowMul(naTemp, i, F.inv(get(get(a,nonZeroidx),j))),i,0,j)
        }
      }
      gaussEliminationItForwardOuter(na,ni,nj)
    }
  }.ensuring(r => rightDimension(r))
  
  def gaussElimination(a: List[List[Boolean]]): List[List[Boolean]] = {
    require(rightDimension(a))
    gaussEliminationItForwardOuter(a,0,0)
  }.ensuring(r => rightDimension(r))

  def gaussEliminationItForwardInnerPreservesZeroKernel(a: List[Boolean], b: List[List[Boolean]], i: BigInt, k: BigInt, j: BigInt): Unit = {
    require(rightDimension(b) && rightRawLength(a) && 0 <= i && i < m && 0 <= j && j < n && 0 <= k && k <= m && frontApply(a, b) == Vm.zero)
    decreases(m-k)
    if(k == m) {}
    else if(k == i) {}
    else {
      getReturnCorrectLength(b,k)
      if (get(get(b,k),j) == F.zeroF) gaussEliminationItForwardInnerPreservesZeroKernel(a, b, i, k+1, j)
      else {
        lemmaRowAddPreservesKer(a, b, i, k, F.neg(get(get(b,k),j)))
        gaussEliminationItForwardInnerPreservesZeroKernel(a, rowAdd(b, i, k, F.neg(get(get(b,k),j))), i, k+1, j)
      }
    }
  }.ensuring(frontApply(a, gaussEliminationItForwardInner(b, i, k, j)) == Vm.zero)

  def gaussEliminationItForwardOuterPreservesZeroKernel(a: List[Boolean], b: List[List[Boolean]], i: BigInt, j: BigInt): Unit = {
    require(rightDimension(b) && rightRawLength(a) && 0 <= i && i <= m && 0 <= j && j <= n && frontApply(a, b) == Vm.zero)
    decreases(m-i+m-j)
    if (i == m || j == n) {}
    else 
    {
      var na = b
      var ni = i+1
      var nj = j+1
      getReturnCorrectLength(b,i)
      if (get(get(b,i),j) == F.oneF) {
        gaussEliminationItForwardInnerPreservesZeroKernel(a, b, i, 0, j)
        na=gaussEliminationItForwardInner(b,i,0,j)
      }
      else if(get(get(b,i),j) != F.zeroF) {
        lemmaRowMulPreservesKer(a, b, i, F.inv(get(get(b,i),j)))
        gaussEliminationItForwardInnerPreservesZeroKernel(a, rowMul(b, i, F.inv(get(get(b,i),j))),i,0,j)
        na=gaussEliminationItForwardInner(rowMul(b, i, F.inv(get(get(b,i),j))),i,0,j)
      }
      else {
        val nonZeroidx = gaussEliminationItForwardFindNonZero(b,i+1,j)
        if(nonZeroidx == -1) {ni=i}
        //Forced to repeat code from above because of measure decreases
        else if(get(get(b,i),j) == F.oneF) {
          lemmaRowSwitchPreservesKer(a, b, i, nonZeroidx)
          gaussEliminationItForwardInnerPreservesZeroKernel(a, rowSwitch(b, i, nonZeroidx),i,0,j)
          na=gaussEliminationItForwardInner(rowSwitch(b, i, nonZeroidx),i,0,j)
        }
        else {
          val naTemp = rowSwitch(b, i, nonZeroidx)
          lemmaRowSwitchPreservesKer(a, b, i, nonZeroidx)
          getReturnCorrectLength(b,nonZeroidx)
          F.InvIsNotZero(get(get(b,nonZeroidx),j))
          lemmaRowMulPreservesKer(a, naTemp, i, F.inv(get(get(b,nonZeroidx),j)))
          gaussEliminationItForwardInnerPreservesZeroKernel(a, rowMul(naTemp, i, F.inv(get(get(b,nonZeroidx),j))),i,0,j)
          na = gaussEliminationItForwardInner(rowMul(naTemp, i, F.inv(get(get(b,nonZeroidx),j))),i,0,j)
        }
      }
      gaussEliminationItForwardOuterPreservesZeroKernel(a,na,ni,nj)
    }
  }.ensuring(frontApply(a, gaussEliminationItForwardOuter(b, i, j)) == Vm.zero) 

  def gaussEliminationPreservesZeroKernel(a: List[Boolean], b: List[List[Boolean]]): Unit = {
    require(rightDimension(b) && rightRawLength(a) && frontApply(a, b) == Vm.zero)
    gaussEliminationItForwardOuterPreservesZeroKernel(a, b, 0, 0)
  }.ensuring(frontApply(a, gaussElimination(b)) == Vm.zero)

  def lemmaCommutativity(a: List[List[Boolean]], b: List[List[Boolean]]): Unit = {
    require(a.length == b.length && a.forall(rightRawLength) && b.forall(rightRawLength))
    (a, b) match {
      case (Cons(x, xs), Cons(y, ys)) =>
        Vn.commutativity(x, y)
        lemmaCommutativity(xs, ys)
      case _ => 
    }
  }.ensuring((sumIt(a, b) == sumIt(b, a)))

  def commutativity(a: List[List[Boolean]], b: List[List[Boolean]]) = {
    require(rightDimension(a) && rightDimension(b))
    lemmaCommutativity(a, b)
    sum(a, b) == sum(b, a)
  }.holds

  def lemmaSumFold(a: List[Boolean], b: List[Boolean], c: List[Boolean]): Unit = {
    require(a.length == b.length && b.length == c.length && a == Vn.sum2(b, c))

    (a, b, c) match {
      case (Cons(x, xs), Cons(y, ys), Cons(z, zs)) =>
        assert(x == Vn.F.sum(y, z))
        lemmaSumFold(xs, ys, zs)
      case _ =>
    }

  }.ensuring(Vn.F.sum(a) == Vn.F.sum(Vn.F.sum(b), Vn.F.sum(c)))

  def lemmaProductDistributivity(a: List[Boolean], b: List[Boolean], c: List[Boolean]): Unit = {
    require(a.length == b.length && b.length == c.length)

    (a, b, c) match {
      case (Cons(x, xs), Cons(y, ys), Cons(z, zs)) =>
        Vn.F.distributivity(x, y, z)
        lemmaProductDistributivity(xs, ys, zs)
      case _ =>
    }

  }.ensuring(product(a, Vn.sum2(b, c)) == Vn.sum2(product(a, b), product(a, c)))

  def lemmaScalarProductDistributivity1(a: List[Boolean], b: List[Boolean], c: List[Boolean]): Unit = {
    require(a.length == b.length && b.length == c.length)
    lemmaProductDistributivity(a, b, c)
    lemmaSumFold(product(a, Vn.sum2(b, c)), product(a, b), product(a, c))
  }.ensuring(scalarProduct(a, Vn.sum2(b, c)) == Vn.F.sum(scalarProduct(a, b), scalarProduct(a, c)))

  def lemmaDistributivity1(a: List[Boolean], b: List[List[Boolean]], c: List[List[Boolean]]): Unit = {
    require(a.length == n && b.length == c.length && b.forall(rightRawLength) && c.forall(rightRawLength))
    (b, c) match {
      case (Cons(x, xs), Cons(y, ys)) =>
        lemmaScalarProductDistributivity1(a, x, y)
        lemmaDistributivity1(a, xs, ys)
      case _ => 
    }
  }.ensuring(frontApplyIt(a, sumIt(b, c)) == Vm.sum2(frontApplyIt(a, b), frontApplyIt(a, c)))

  def distributivity1(a: List[Boolean], b: List[List[Boolean]], c: List[List[Boolean]]) = {
    require(a.length == n && rightDimension(b) && rightDimension(c))
    lemmaDistributivity1(a, b, c)
    frontApply(a, sum(b, c)) == Vm.sum(frontApply(a, b), frontApply(a, c))
  }.holds

  def lemmaScalarProductDistributivity2(a: List[Boolean], b: List[Boolean], c: List[Boolean]): Unit = {
    require(a.length == b.length && b.length == c.length)

    (a, b, c) match {
      case (Cons(x, xs), Cons(y, ys), Cons(z, zs)) =>
        assert(Vn.F.mul(Vn.F.sum(x, y), z) == Vm.F.sum(Vn.F.mul(x, z), Vn.F.mul(y, z)))
        lemmaScalarProductDistributivity2(xs, ys, zs)
      case _ =>
    }

  }.ensuring(scalarProduct(Vn.sum2(a, b), c) == Vm.F.sum(scalarProduct(a, c), scalarProduct(b, c)))

  def lemmaDistributivity2(a: List[Boolean], b: List[Boolean], c: List[List[Boolean]]): Unit = {
    require(a.length == n && b.length == n && c.forall(rightRawLength))

    c match {
      case (Cons(x, xs)) =>
        lemmaScalarProductDistributivity2(a, b, x)
        lemmaDistributivity2(a, b, xs)
      case _ =>
    }

  }.ensuring(frontApplyIt(Vn.sum2(a, b), c) == Vm.sum2(frontApplyIt(a, c), frontApplyIt(b, c)))

  def distributivity2(a: List[Boolean], b: List[Boolean], c: List[List[Boolean]]) = {
    require(a.length == n && b.length == n && rightDimension(c))
    lemmaDistributivity2(a, b, c)
    frontApply(Vn.sum(a, b), c) == Vm.sum(frontApply(a, c), frontApply(b, c))
  }.holds

  def scalarProductZeroIsZero(a: List[Boolean], b: List[Boolean]): Unit = {
    require(a.length == b.length && b.forall(v => v == Vn.F.zeroF))

    (a, b) match {
      case (Cons(x, xs), Cons(y, ys)) =>
        Vn.F.mulZero(y)
        scalarProductZeroIsZero(xs, ys)
      case _ =>
    }

  }.ensuring(scalarProduct(a, b) == Vn.F.zeroF)

  def innerItNegHasOnlyZero(i: BigInt, a: List[Boolean]): Unit = {
    require(i < 0 && innerIt(i, a))

    a match {
      case Cons(x, xs) =>
        innerItNegHasOnlyZero(i-1, xs)
      case Nil() =>
    }

  }.ensuring(a.forall(v => v == Vn.F.zeroF))

  def scalarProductIdentityVecIsOnlyOneProduct(a: List[Boolean], b: List[Boolean], aI: Boolean, bI: Boolean): Unit = {
    require(a.length > 0 && a.length == b.length && innerIt(0, b) && get(a, 0) == aI && get(b, 0) == bI)
    innerItNegHasOnlyZero(-1, b.tail)
    scalarProductZeroIsZero(a.tail, b.tail)
  }.ensuring(scalarProduct(a, b) == Vn.F.mul(aI, bI))

  def checkScalarProductZero(a: List[Boolean], aI: Boolean, b: List[Boolean], i: BigInt): Unit = {
    require(a.length == b.length && scalarProduct(a, b) == Vn.F.zeroF && innerIt(i, b) && i >= 0 && i < a.length && get(a, i) == aI)

    (a, b) match {
      case (Cons(x, xs), Cons(y, ys)) if i > 0 => 
        checkScalarProductZero(xs, aI, ys, i - 1)
      case (Cons(x, xs), Cons(y, ys)) if i == 0 =>
        scalarProductIdentityVecIsOnlyOneProduct(a, b, x, y)
        Vn.F.zeroProd(x, y)
      case _ =>
    }

  }.ensuring(aI == Vn.F.zeroF)

  def subVector(a: List[Boolean], b: List[Boolean]): Boolean = {
    require(a.length >= b.length)
    (a, b) match {
      case (Cons(x, xs), Cons(y, ys)) if (a.length > b.length) =>
        subVector(xs, b)
      case (Cons(x, xs), Cons(y, ys)) if (a.length == b.length) =>
        x == y && subVector(xs, ys)
      case _ => true
    }
  }

  def subVectorRefl(a: List[Boolean]): Unit = {
    a match {
      case Cons(x, xs) => subVectorRefl(xs)
      case Nil() =>
    }
  }.ensuring(subVector(a, a))

  def subVectorGetSame(a: List[Boolean], b: List[Boolean], i: BigInt): Unit = {
    require(a.length >= b.length && subVector(a, b) && i == a.length - b.length && b.length > 0)

    a match {
      case Cons(x, xs) => if (a.length > b.length) subVectorGetSame(xs, b, i - 1)
      case Nil() =>
    }

  }.ensuring(get(a, i) == b.head)

  def subVectorTail(a: List[Boolean], b: List[Boolean]): Unit = {
    require(a.length >= b.length && subVector(a, b) && b.length > 0)

    (a, b) match {
      case (Cons(x, xs), Cons(y, ys)) if (a.length > b.length) => subVectorTail(xs, b)
      case (Cons(x, xs), Cons(y, ys)) if (a.length == b.length) =>
    }

  }.ensuring(subVector(a, b.tail))

  def standardFormIsFullRankIt(a: List[Boolean], aIt: List[Boolean], i: BigInt, b: List[List[Boolean]]): Unit = {
    require(i >= 0 && i <= n && a.length == n && aIt.length == n - i && b.length == m - i && b.forall(rightRawLength) && subVector(a, aIt) && outerIt(i, b) &&
    frontApplyIt(a, b) == List.fill(b.length)(Vn.F.zeroF))

    (aIt, b) match {
      case (Cons(x, xs), Cons(y, ys)) if i < n =>
        subVectorGetSame(a, aIt, i)
        checkScalarProductZero(a, x, y, i)
        subVectorTail(a, aIt)
        standardFormIsFullRankIt(a, xs, i + 1, ys)
      case (Nil(), _) if i == n =>
    }
  }.ensuring(aIt == List.fill(n - i)(Vn.F.zeroF))

  def standardFormIsFullRank(a: List[Boolean], b: List[List[Boolean]]): Unit = {
    require(rightDimension(b) && rightRawLength(a) && isStandardForm(b) && frontApply(a, b) == Vm.zero)
    subVectorRefl(a)
    standardFormIsFullRankIt(a, a, 0, b)
  }.ensuring(a == Vn.zero)
}
