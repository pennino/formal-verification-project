import stainless.lang._

abstract class A {
  def x(a: Int): Int
}

case object B extends A {
  def x(a: Int) = {
    require(a > 0)
    42
  } ensuring { _ >= 0 }
}